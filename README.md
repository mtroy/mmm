# MMM

Multi Mount Manager is a simple tool designed to easily manage storage disk over rack numeration.

Most of time, you're rack slot ID are recognizable from hw and visually in front of the bays, but to avoid harmful issue at umount/extract, 
i decide to use named dirs combinating disk name / visual rack id as mounting path.
The tool provide a state viewer and few func to operate on it properly.
(it happily replaces stickers !)

## Usage

#### mount
```sh
$> mu /dev/sdc1 /mnt/storage/r2
 [storage] ► is racked
```

#### umount
```sh
$> um storage
 Done!
 [storage] ► is racked out
```

#### list / alias
###### List actives mount points
```sh
$> mm
N  HASH       MOUNTPOINT     RACK  DEV
►  [storage]  storage/r2/    [r2]  sdc1
```
##### make an alias path
```sh
$> mm usb
 [/mnt/storage/r2] ► released on current term
$> cd ~storage/
$> pwd
 /mnt/storage/r2
```

## Examples of structure

```sh
#### simple
/mnt
├── rack_0
├── rack_1
├── rack_2
├── rack_3

#### more complex
/mnt
├── usb
│   ├── b0
│   ├── b1
│   └── b2
├── anotherDisk
│   ├── r0
│   ├── r1
│   ├── r2
│   └── r3
├── storage
│   ├── r0
│   ├── r1
│   ├── r2
│   └── r3
└── movies
    ├── r0
    ├── r1
    ├── r2
    └── r3
```
