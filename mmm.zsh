#!/bin/zsh

## version 0.1

## Unmount
um()
{
	local mp=$(mount | grep "$1/[r|b]" | tail -1 | awk '{print $3}')
	sudo umount $mp;
	unhash -d $1
	rm /home/$USER/.mm/$(echo $1|tr -d '~')
	echo "[$1] ► is racked out"
}

## Mount
mu()
{
	[[ -d $2 && $2 =~ "[r|b][0-5]$" ]] && # ensure racknumber in path
	{
		sudo mount -o noatime $*;
		local h=$(echo $2 | awk -F'/' '{print $(NF-1)}')
		touch /home/$USER/.mm/$h;
		echo "[$h] ► is racked"
	} || echo "[$2] ► is not a valid rack point!"
}
alias mn='cd /mnt'

## Status
mm()
{
	#echo $funcstack[1]; $funcstack[-1]

	# hashpoint
	local h=$1

	# racklist
	local mmdir="/home/$USER/.mm"

	# mount path
	local mp=$(mount | grep "$h/[r|b]" | tail -1 | awk '{print $3}')

	# rack number
	local rk=$(echo $mp| awk -F'/' '{print $NF}')

	# hash value
	local hv=$(hash -d | awk -F'/' -v pattern="$h" '$0 ~ pattern {print $NF}')
#	local ee=$(hash -d | awk -F'=' -v pato="$h" '$0 ~ pato {print $1}')
#		echo "$h, $mp, $mmdir, $rk - $hv :: $ee";


	[[ $# -eq 0 || -z "$1" ]] &&
	{
#		echo "listing"
		local racklist=(${(ps:\n:)"$(mount | grep /mnt | awk '{print $1 $3}')"})
		[[ ${#racklist[@]} -ne 0 ]] &&
		{
			local table
			for p in $racklist;
			do 
				[[  $p =~ "/mnt/(.*)/[r|b][0-5]$" ]] && 
				{
#					for(i=5; i<=NF; i++) printf $i "::"FS;
					table+=$(echo $p | awk -F'/' '{printf "► ["$(NF-1)"] "; for(i=5; i<=NF; i++) printf $i FS; print " ["$NF"] "$3}')
					table+="\n"
				}
			done
#			echo $table
			echo $table | column --table --table-columns N,HASH,MOUNTPOINT,RACK,DEV -R N --table-hide -
#			echo $table | column --table -s$'\t' 

		} || echo ":( all rack are empty"

	} || # hash-mount operations
	{

		[[ -d $mp ]] && # is racked
		{
			[[ -a "$mmdir/$h" ]] && # has been released
			{
				# replicate hash if older or not env-released
				[[ $h != $(hash -d | awk -F'=' -v pattern="$h" '$0 ~ pattern {print $1}') || $rk != $hv ]] &&
				{
					hash -d $h=$mp;
	#				echo $(hash -d | awk -F'=' '/$h/ {print $1}');
					echo "[$mp] ► released on current term";
				} || echo "[$mp] ► is already hashed on current term"

			} || { touch "$mmdir/$h"; cd $mp; hash -d $h=$mp } # release and move
		} || echo "[$h] ► is not part of racklist"
	}
}

